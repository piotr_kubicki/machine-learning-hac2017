# Vehicle Accidents Predictions Experiment

The purpose of this experiment was to investigate possibility of using publicly
available data of vehicle accidents in UK to predict how serious an accident
will be for an individual.

Some results presented in this experiment shown very good results of predictions
for some train models, but due to short time available during Napier University
School of Computing Hackathon 2017, it wasn't properly investigated and
evaluated.

Some additional work is expected to be undertaken in the near future.

Data downloaded from: https://data.gov.uk/
