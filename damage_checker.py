import tensorflow as tf

# read training exaples
filename_queue = tf.train.string_input_producer(['casualities.csv'], shuffle=True)
reader = tf.TextLineReader()
key, value = reader.read(filename_queue)

record_defaults = [[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0]]
col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13 = tf.decode_csv(value, record_defaults=record_defaults)

features = tf.stack([col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12])

with tf.Session() as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    for i in range(1200):
        example, label = sess.run([features, col13])

    coord.request_stop()
    coord.join(threads)

print coord
# input values
# x = tf.placeholder(tf.float32, [None, 12])
#
# # Weights
# W = tf.Variable(tf.zeros([12, 3]))
# b = tf.Variable(tf.zeros([3]))
#
# # model
# y = tf.nn.softmax(tf.matmul(x, W) + b)
#
# y_ = tf.placeholder(tf.float32, [None, 3])
# cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
#
# train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
#
# sess = tf.InteractiveSession()
# tf.global_variables_initializer().run()
#
# for _ in range(1000):
#     batch_xs, batch_ys = mnist.train.next_batch(100)
#     sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
#
# correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
#
# accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
