import pandas as pd
import numpy as np
import random as rnd

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn import model_selection

examples = pd.read_csv('casualities.csv')
print(examples.columns.values)

X_train = examples.drop('Casualty_Severity', axis=1)
Y_train = examples['Casualty_Severity']
test_size = 0.33
seed = 7

X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X_train, Y_train, test_size=test_size, random_state=seed)
# X_train, X_test = X_train[:120000], X_train[:60000]
# Y_train, Y_test = Y_train[:120000], Y_train[:60000]

print '{} {} {}'.format(X_train.shape, Y_train.shape, X_test.shape)

# logreg = LogisticRegression(solver='newton-cg', multi_class='multinomial', max_iter=1000)
# logreg.fit(X_train, Y_train)
# Y_pred = logreg.predict(X_test)
# print Y_pred.value_counts()
# acc_log = round(logreg.score(X_train, Y_train) * 100, 2)
# print acc_log
# mlp = MLPClassifier()
# mlp.fit(X_train, Y_train)
#
# res = mlp.predict_proba(X_test)
# values, counts = np.unique(res, return_counts=True)
# print dict(zip(values, counts))
# acc_mlp = round(mlp.score(X_train, Y_train) * 100, 2)
# print acc_mlp

# decision_tree = DecisionTreeClassifier()
# decision_tree.fit(X_train, Y_train)
# Y_pred = decision_tree.predict(X_test)
# values, counts = np.unique(Y_pred, return_counts=True)
# print dict(zip(values, counts))
# acc_decision_tree = round(decision_tree.score(X_train, Y_train) * 100, 2)
# print acc_decision_tree

# gaussian = GaussianNB()
# gaussian.fit(X_train, Y_train)
# Y_pred = gaussian.predict(X_test)
# acc_gaussian = round(gaussian.score(X_train, Y_train) * 100, 2)
# print acc_gaussian
# values, counts = np.unique(Y_pred, return_counts=True)
# print dict(zip(values, counts))

# knn = KNeighborsClassifier(n_neighbors = 3)
# knn.fit(X_train, Y_train)
# Y_pred = knn.predict(X_test)
# acc_knn = round(knn.score(X_train, Y_train) * 100, 2)
# print acc_knn
# values, counts = np.unique(Y_pred, return_counts=True)
# print dict(zip(values, counts))

logreg = LogisticRegression(solver='newton-cg', multi_class='multinomial', max_iter=1000)
logreg.fit(X_train, Y_train)
Y_pred = logreg.predict(X_test)
acc_log = round(logreg.score(X_train, Y_train) * 100, 2)
print acc_log
values, counts = np.unique(Y_pred, return_counts=True)
print dict(zip(values, counts))
